import React from 'react';

function activeClickHandler(props) {
    let count = props.state.tasks.reduce((count, current) => count + !current.check, 0);

    props.setCount(count);
    props.setBtn("Active");
}

const BtnActive = (props) => {
    return (
        <div onClick={() => activeClickHandler(props)}>
            Active
        </div>
    )
}

export default BtnActive;


// let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);