import React from "react";
import classes from "./input.module.css";

function keyUpHandler(props, event) {
    let state = props.state,
        setState = props.setState;

    if (event.keyCode !== 13) {

        setState({
            tasks: state.tasks,
            text: event.target.value,
        });

    } else if (state.text !== "") {
        let mas = state.tasks;

        let task = {
            text: state.text,
            check: false,
            id: mas.length !== 0 ? Math.max(...(mas.map((current) => current.id))) + 1 : 0,
        };

        mas.unshift(task);

        setState({
            tasks: state.tasks,
            text: event.target.value,
        });

        props.setCount(props.count + 1);

        event.target.value = "";
    }
}

const Input = (props) => {
    return (
        <div className={classes.inputContainer}>
            <input type="text" className={classes.input} placeholder="Enter task" onKeyUp={ (event) => { keyUpHandler(props, event) } } />
        </div>
    )
}

export default Input;