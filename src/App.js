import React, { useState } from 'react';
import Input from "./components/input/Input";
import Tasks from "./components/tasks/Tasks";
import Buttons from "./components/buttons/buttons";

function App() {
  const [state, setState] = useState({tasks: [], text: ""});
  const [count, setCount] =  useState(0);
  const [activeBtn, setBtn] = useState("All");

  return (
    <div className="container">
        <Input state={state} setState={setState} setCount={setCount} count={count} />
        <Tasks state={state} setState={setState} setCount={setCount} count={count} />
        <Buttons state={state} setState={setState} count={count} setCount={setCount} activeBtn={activeBtn} setBtn={setBtn} />
    </div>
  );
}

export default App;