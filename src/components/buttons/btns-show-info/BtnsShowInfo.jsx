import React from 'react';
import classes from "./btns-show.module.css"
import BtnAll from "./btn-all/BtnAll";
import BtnActive from "./btn-active/BtnActive";
import BtnCompleted from "./btn-completed/BtnCompleted";

const BtnsShowInfo = (props) => {
    return (
        <div className={classes.btnsShowInfo}>
            <BtnAll state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} />
            <BtnActive state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} />
            <BtnCompleted state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} />
        </div>
    )
}

export default BtnsShowInfo;