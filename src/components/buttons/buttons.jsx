import React from 'react';
import classes from "./buttons.module.css"
import BtnInfo from "./btn-info/BtnInfo";
import BtnDelete from "./btn-delete/BtnDelete";
import BtnsShowInfo from "./btns-show-info/BtnsShowInfo";

const Buttons = (props) => {
    return (
        <div className={classes.buttonsContainer}>
            <BtnInfo count={props.count} activeBtn={props.activeBtn} />
            <BtnsShowInfo state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} />
            <BtnDelete state={props.state} setState={props.setState} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} />
        </div>
    )
}

export default Buttons;