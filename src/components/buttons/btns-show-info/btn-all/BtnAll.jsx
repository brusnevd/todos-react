import React from 'react';

function allClickHandler(props) {
    props.setCount(props.state.tasks.length);
    props.setBtn("All");
}

const BtnAll = (props) => {
    return (
        <div onClick={() => allClickHandler(props)}>
            All
        </div>
    )
}

export default BtnAll;






// completed --------------------------------------------------------------------------
// let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);