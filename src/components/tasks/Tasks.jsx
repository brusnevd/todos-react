import React from 'react';
import classes from "./tasks.module.css"

function deleteTask(event, props) {
    let mas = props.state.tasks.filter((current) => {
        return "del" + current.id !== event.target.id;
    });

    console.log(mas)

    props.setState(
        {
            text: "",
            tasks: mas,
        }
    );

    // props.setCount(props.count - 1);
}

function checkboxChange(event, props) {
    let mas = props.state.tasks.map((current) => {
        if ("checkbox" + current.id === event.target.id) {
            if (current.check === false) {
                current.check = true;
            } else {
                current.check = false;
            }
        }

        return current;
    });

    props.setState(
        {
            text: "",
            tasks: mas,
        }
    );
}

const Tasks = (props) => {
    let tasksMas = props.state.tasks.map((current) => 
        <div key={current.id} className={classes.task} >
            <input type="checkbox" id={"checkbox" + current.id} className={classes.checkbox} defaultChecked={current.check} onClick={(event) => checkboxChange(event, props)} />
            <label htmlFor={"checkbox" + current.id} className={classes.taskText} onClick={(event) => checkboxChange(event, props)} > {current.text} </label>
            <span className={classes.delete} onClick={(event) => deleteTask(event, props)} id={"del" + current.id}>&#10006;</span>
        </div> 
    );

    return (
        <div className={classes.tasksContainer}>
            {tasksMas}
        </div>
    )
}

export default Tasks;