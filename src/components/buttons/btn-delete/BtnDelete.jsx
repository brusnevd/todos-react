import React from 'react';
import classes from "./btn-delete.module.css"

function deleteTasks(props) {
    let mas;

    if (props.activeBtn === "All") {
        mas = [];
    }

    if (props.activeBtn === "Active") {
        mas = props.state.tasks.filter((current) => {
            return current.check === true;
        });
    }

    if (props.activeBtn === "Completed") {
        mas = props.state.tasks.filter((current) => {
            return current.check !== true;
        });
    }

    props.setState(
        {
            text: "",
            tasks: mas,
        }
    );
}

const BtnDelete = (props) => {
    return (
        <div className={classes.btnDelete} onClick={() => deleteTasks(props)}>
            Delete {props.activeBtn} tasks
        </div>
    )
}

export default BtnDelete;