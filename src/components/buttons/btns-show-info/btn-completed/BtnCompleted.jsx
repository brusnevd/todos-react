import React from 'react';

function completedClickHandler(props) {
    let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);

    props.setCount(count);
    props.setBtn("Completed");
}

const BtnCompleted = (props) => {
    return (
        <div onClick={() => completedClickHandler(props)}>
            Completed
        </div>
    )
}

export default BtnCompleted;